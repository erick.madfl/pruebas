<?php 
require 'connection.db.php';

class Model{

    public $database = '';

    public function __construct(){
        $this->database = new Database();
    }

    public function allUsers(){
        $respuesta = null; 
        try{
            $sql = "SELECT * FROM usuarios WHERE activo = 1";
            $db = $this->database->getConnection();
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $respuesta["status"] = 200;
            $respuesta["mensaje"] = "ok";
            $respuesta["data"] = $data;
        }catch(PDOException $e){
            $respuesta["status"] = 400;
            $respuesta["mensaje"] = $e->getMessage();
            $respuesta["data"] = [];
        }
        return $respuesta;
       
    }
    public function addUsr($nombre, $paterno, $edad){

        $respuesta = null;

        try{

            $sql = "INSERT INTO usuarios (nombre, paterno, edad, activo, creacion) values(:nombre, :paterno, :edad, :activo, :creacion)";
            
            $db = $this->database->getConnection();
            $activo = 1;
            $creacion = date("Y-m-d H:i:s");
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":nombre", $nombre);
            $stmt->bindParam(":paterno", $paterno);
            $stmt->bindParam(":edad", $edad);
            $stmt->bindParam(":activo", $activo);
            $stmt->bindParam(":creacion", $creacion);
            $stmt->execute();
            $respuesta["status"] = 200;
            $respuesta["mensaje"] = "Se ha regitrado correctamente";
        }catch(PDOException $e){
            $respuesta["status"] = 400;
            $respuesta["mensaje"] = $e->getMessage();
        }

        return $respuesta;
    }

    public function updateUsr($nombre, $paterno, $edad, $id){

        $respuesta = null;

        try{

            $sql = "UPDATE usuarios SET nombre = :nombre, paterno = :paterno, edad = :edad, modificacion = :modificacion WHERE id = :id";
            $modificacion = date("Y-m-d H:i:s");
            
            $db = $this->database->getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id", $id);
            $stmt->bindParam(":nombre", $nombre);
            $stmt->bindParam(":paterno", $paterno);
            $stmt->bindParam(":edad", $edad);
            $stmt->bindParam(":modificacion", $modificacion);
            $stmt->execute();
            $respuesta["status"] = 200;
            $respuesta["mensaje"] = "Se ha Actualizado correctamente";
        }catch(PDOException $e){
            $respuesta["status"] = 400;
            $respuesta["mensaje"] = $e->getMessage();
        }

        return $respuesta;
    }

    public function deleteUsr($id){

        $respuesta = null;

        try{

            $sql = "UPDATE usuarios SET activo = 0, eliminacion = :eliminacion WHERE id = :id";
            $eliminacion = date("Y-m-d H:i:s");
            
            $db = $this->database->getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(":id", $id);
            $stmt->bindParam(":eliminacion", $eliminacion);
            
            $stmt->execute();
            $respuesta["status"] = 200;
            $respuesta["mensaje"] = "Se ha eliminado correctamente";
        }catch(PDOException $e){
            $respuesta["status"] = 400;
            $respuesta["mensaje"] = $e->getMessage();
        }

        return $respuesta;
    }
}