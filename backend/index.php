<?php 
require 'model.db.php';


$model = new Model();


$respuesta = null;

$opcion=(isset($_POST["opcion"]))?$_POST["opcion"]:'';
// Habilitar CORS
header("Access-Control-Allow-Origin: *"); // Permite solicitudes desde cualquier origen
header("Access-Control-Allow-Methods: GET, POST, OPTIONS"); // Define los métodos HTTP permitidos
header("Access-Control-Allow-Headers: Content-Type"); // Define los encabezados permitidos

// Manejo de solicitudes OPTIONS (preflight)
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
    header("Access-Control-Allow-Headers: Content-Type");
    exit;
}

switch( $opcion ){
    case "generarUsuarios":
        $inicio = microtime(true);
        $nombres = [
			"Aaron","Abdon","Abel","Abelardo","Abrahan","Absalon","Acacio","Adalberto",
			"Adan","Adolfo","Adon","Adrian","Agustin","Aitor","Albert","Alberto","Alejandro",
			"Alejo","Alfonso","Alfredo","Alicia","Alipio","Alonso","Alvaro","Amadeo","Amaro",
			"Ambrosio","Amparo","Anatolio","Andres","Angel","Angeles","Aniano","Anna","Anselmo",
			"Antero","Antonio","Aquiles","Aranzazu","Arcadio","Aresio","Aristides","Arnaldo",
			"Artemio","Arturo","Atanasio","Augusto","Aureliano","Aurelio","Baldomero","Balduino",
			"Baltasar","Bartolome","Basileo","Beltran","Benedicto","Benigno","Benito","Benjamin",
			"Bernabe","Bernardo","Blas","Bonifacio","Bruno","Calixto","Camilo","Carlos","Carmelo",
			"Casiano","Casimiro","Casio","Cayetano","Cayo","Ceferino","Celso","Cesar","Cesareo",
			"Cipriano","Cirilo","Cirino","Ciro","Claudio","Cleofas","Colombo","Columba","Columbano",
			"Conrado","Constancio","Constantino","Cosme","Cristian","Cristobal","Daciano","Dacio",
			"Damaso","Damian","Daniel","Dario","David","Democrito","Diego","Dimas","Dolores","Domingo",
			"Donato","Edgar","Edmundo","Eduardo","Eduvigis","Efren","Elias","Eliseo","Emiliano",
			"Emilio","Encarnacion","Enrique","Erico","Ernesto","Esdras","Esiquio","Esteban","Eugenio",
			"Eusebio","Evaristo","Ezequiel","Fabian","Fabio","Facundo","Faustino","Fausto","Federico",
			"Feliciano","Felipe","Felix","Fermin","Fernando","Fidel","Fortunato","Francesc","Francisco",
			"Fulgencio","Gabriel","Gerardo","German","Godofredo","Gonzalo","Gregorio","Guido","Guillermo",
			"Gustavo","Guzman","Hector","Heliodoro","Heraclio","Heriberto","Hilarion","Homero","Honorato",
			"Honorio","Hugo","Humberto","Ifigenia","Ignacio","Ildefonso","Inocencio","Ireneo","Isaac",
			"Isaias","Isidro","Ismael","Ivan","Jacinto","Jacob","Jacobo","Jaime","Jaume","Javier","Jeremias",
			"Jeronimo","Jesus","Joan","Joaquim","Joaquin","Joel","Jonas","Jonathan","Jordi",
			"Jorge","Josafat","Jose","Josep","Josue","Juan","Julia","Julian","Julio","Justino",
			"Juvenal","Ladislao","Laureano","Lazaro","Leandro","Leon","Leonardo","Leoncio","Leonor",
			"Leopoldo","Lino","Lorenzo","Lourdes","Lucano","Lucas","Luciano","Luis","Luz",
			"Macario","Manuel","Mar","Marc","Marcelino","Marcelo","Marcial","Marciano","Marcos",
			"Maria","Mariano","Mario","Martin","Mateo","Matias","Mauricio","Maximiliano","Melchor",
			"Miguel","Miqueas","Mohamed","Moises","Narciso","Nazario","Nemesio","Nicanor",
			"Nicodemo","Nicolas","Nicomedes","Noe","Norberto","Octavio","Odon","Onesimo","Orestes",
			"Oriol","Oscar","oscar","Oseas","Oswaldo","Oto","Pablo","Pancracio","Pascual","Patricio",
			"Pedro","Pio","Poncio","Porfirio","Primo","Probo","Rafael","Raimundo","Ramiro","Ramon",
			"Raul","Reinaldo","Renato","Ricardo","Rigoberto","Roberto","Rocio","Rodrigo","Rogelio",
			"Roman","Romualdo","Roque","Rosendo","Ruben","Rufo","Ruperto","Salomon","Salvador",
			"Salvio","Samuel","Sanson","Santiago","Sebastian","Segismundo","Sergio","Severino",
			"Simeon","Simon","Siro","Sixto","Tadeo","Tarsicio","Teodora","Teodosia","Teofanes",
			"Timoteo","Tito","Tobias","Tomas","Tomas","Toribio","Ubaldo","Urbano","Valentin","Valeriano",
			"Velerio","Venancio","Vicente","Victor","Victorino","Victorio","Virgilio","Vladimiro","Wilfredo",
			"Xavier","Zacarias","Zaqueo","Adela","Adelaida","Alba","Albina","Alejandra","Almudena","Amelia","Ana",
			"Anastasia","Andrea","Angela","Ananias","Antonia","Araceli","Ariadna",
			"Ascension","Asuncion","Aurea","Aurelia","Aurora","Barbara","Beatriz",
			"Belen","Bernarda","Blanca","Borja","Candida","Carina","Carmen","Carolina",
			"Catalina","Cecilia","Celia","Celina","Clara","Claudia","Clotilde",
			"Concepcion","Consuelo","Cristina","Dorotea","Elena","Elisa","Elvira",
			"Emilia","Epifania","Esperanza","Ester","Esther","Eugenia","Eulalia",
			"Eva","Fabiola","Fatima","Francisca","Gema","Genoveva","Gertrudis",
			"Gisela","Gloria","Guadalupe","Hildegarda","Ines","Inmaculada","Irene",
			"Isabel","Josefa","Josefina","Juana","Laura","Leocadia","Lidia","Liduvina",
			"Lorena","Lucia","Lucrecia","Luisa","Magdalena","Manuela","Margarita",
			"Marina","Marta","Matilde","Mercedes","Milagros","Miriam","Monica",
			"Montserrat","Natalia","Natividad","Nieves","Noelia","Nuria","Olga",
			"Otilia","Patricia","Paula","Petronila","Pilar","Priscila","Purificacion",
			"Raquel","Rebeca","Remedios","Rita","Rosa","Rosalia","Rosario","Salome",
			"Sandra","Sara","Silvia","Sofia","Soledad","Sonia","Susana","Tania","Teofila",
			"Teresa","Trinidad","Ursula","Vanesa","Veronica","Vicenta","Victoria",
			"Vidal","Virginia","Yolanda"
	    ];
        $apellidos = [
			"Aguilar","Alonso","Alvarez","Arias","Benitez","Blanco","Blesa","Bravo",
			"Caballero","Cabrera","Calvo","Cambil","Campos","Cano","Carmona","Carrasco",
			"Castillo","Castro","Cortes","Crespo","Cruz","Delgado","Diaz","Diez","Dominguez",
			"Duran","Esteban","Fernandez","Ferrer","Flores","Fuentes","Gallardo","Gallego",
			"Garcia","Garrido","Gil","Gimenez","Gomez","Gonzalez","Guerrero","Gutierrez",
			"Hernandez","Herrera","Herrero","Hidalgo","Iglesias","Jimenez","Leon","Lopez",
			"Lorenzo","Lozano","Marin","Marquez","Martin","Martinez","Medina","Mendez",
			"Molina","Montero","Montoro","Mora","Morales","Moreno","Moya","Navarro","Nieto",
			"Ortega","Ortiz","Parra","Pascual","Pastor","Perez","Prieto","Ramirez","Ramos",
			"Rey","Reyes","Rodriguez","Roman","Romero","Rubio","Ruiz","Saez","Sanchez",
			"Santana","Santiago","Santos","Sanz","Serrano","Soler","Soto","Suarez",
			"Torres","Vargas","Vazquez","Vega","Velasco","Vicente","Vidal"
		];
        $personas = [];
        $maxPersonas = 10; 
        $contador = 0;
        
        while ($contador < $maxPersonas) {
            $nombre = $nombres[array_rand($nombres)]; 
            $apellido = $apellidos[array_rand($apellidos)]; 
            $nombreCompleto = $nombre . " " . $apellido;
            
            if (!in_array($nombreCompleto, $personas)) {
                $personas[] = $nombreCompleto;
                $contador++;
            }
        }
        
        foreach ($personas as $persona) {
            $edad = mt_rand(1, 99);
            $nombreApellido = explode(" ", $persona);
            $model->addUsr($nombreApellido[0], $nombreApellido[1], $edad);
        }
        $fin = microtime(true);
        $tiempoDeRespuesta = ($fin - $inicio) * 1000;
        $respuesta = array(
            "status" => 200,
            "message" => "ok",
            "timeStamp" => date("Y-m-d H:i:s"),
            "tiempoRespuesta" => $tiempoDeRespuesta."ms",
            "data" => $model->allUsers()
        );

    break;

    case "todosUsuarios":
        $inicio = microtime(true);
        try {
            $data = $model->allUsers();
            if ($data['status'] == 400) {
                throw new Exception($data['mensaje']);
            }
            $fin = microtime(true);
            $tiempoDeRespuesta = ($fin - $inicio) * 1000;
            $respuesta = array(
                "status" => 200,
                "message" => "ok",
                "timeStamp" => date("Y-m-d H:i:s"),
                "tiempoRespuesta" => $tiempoDeRespuesta."ms",
                "data" => $data
            );

        } catch (Exception $e) {
            $fin = microtime(true);
            $tiempoDeRespuesta = ($fin - $inicio) * 1000;
            $respuesta = array(
                "status" => 400,
                "message" => $e->getMessage(),
                "timeStamp" => date("Y-m-d H:i:s"),
                "tiempoRespuesta" => $tiempoDeRespuesta."ms",
                "data" => []
            );
        }
    break;

    case "modificarUsuario":
        $inicio = microtime(true);
        try {
                      
            $arrayErrores = [];
            foreach ($_POST as $pt) {
                if ($pt == '') {
                    array_push($arrayErrores, $pt." es un campo requerido");
                }
            }

            if(count($arrayErrores) > 0 ){
                throw new Exception(json_encode($arrayErrores));
            }
             
            $nombre = $_POST["nombre"];
            $paterno = $_POST["paterno"];
            $edad = $_POST["edad"];
            $id = $_POST["id"];

            $data = $model->updateUsr($nombre, $paterno, $edad, $id);
            if ($data['status'] == 400) {
                throw new Exception($data['mensaje']);
            }
            $fin = microtime(true);
            $tiempoDeRespuesta = ($fin - $inicio) * 1000;
            $respuesta = array(
                "status" => 200,
                "message" => "ok",
                "timeStamp" => date("Y-m-d H:i:s"),
                "tiempoRespuesta" => $tiempoDeRespuesta."ms",
                "data" => $data
            );

        } catch (Exception $e) {
            $fin = microtime(true);
            $tiempoDeRespuesta = ($fin - $inicio) * 1000;
            $respuesta = array(
                "status" => 400,
                "message" => $e->getMessage(),
                "timeStamp" => date("Y-m-d H:i:s"),
                "tiempoRespuesta" => $tiempoDeRespuesta."ms",
                "data" => []
            );
        }
    break;

    case "eliminarUsuario":
        $inicio = microtime(true);
        try {
            $id = $_POST["id"];
            if($id == ''){
                throw new Exception("el ID es requerido");
            }
            $data = $model->deleteUsr($id);
            if ($data['status'] == 400) {
                throw new Exception($data['mensaje']);
            }
            $fin = microtime(true);
            $tiempoDeRespuesta = ($fin - $inicio) * 1000;
            $respuesta = array(
                "status" => 200,
                "message" => "ok",
                "timeStamp" => date("Y-m-d H:i:s"),
                "tiempoRespuesta" => $tiempoDeRespuesta."ms",
                "data" => $data
            );

        } catch (Exception $e) {
            $fin = microtime(true);
            $tiempoDeRespuesta = ($fin - $inicio) * 1000;
            $respuesta = array(
                "status" => 400,
                "message" => $e->getMessage(),
                "timeStamp" => date("Y-m-d H:i:s"),
                "tiempoRespuesta" => $tiempoDeRespuesta."ms",
                "data" => []
            );
        }
    break;

    default:
        $inicio = microtime(true);
        $fin = microtime(true);
        $tiempoDeRespuesta = ($fin - $inicio) * 1000;

        $respuesta = array(
            "status" => 200,
            "message" => "La aplicacion se esta ejecutando en la versión 1.0.0 ",
            "timeStamp" => date("Y-m-d H:i:s"),
            "tiempoRespuesta" => $tiempoDeRespuesta."ms",
            "data" => array()
        );
        
        http_response_code(200);

}

echo json_encode($respuesta);


 
?>